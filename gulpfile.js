const gulp = require('gulp');
const sass = require('gulp-sass');
var browserify = require('gulp-browserify');
const autoprefixer = require('gulp-autoprefixer');

gulp.task('styles', function() {
    return gulp.src('sass/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 99 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('./css/'));
});

gulp.task('jsme', function () {
    return  gulp.src('js/*.js')
        .pipe(browserify())
        .pipe(gulp.dest('./js_output'));
});
